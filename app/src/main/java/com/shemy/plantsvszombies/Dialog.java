package com.shemy.plantsvszombies;

import org.cocos2d.layers.CCLayer;
import org.cocos2d.layers.CCScene;
import org.cocos2d.menus.CCMenu;
import org.cocos2d.menus.CCMenuItemSprite;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCLabel;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.transitions.CCFadeTransition;
import org.cocos2d.types.ccColor3B;

public class Dialog extends CCLayer {

    public Dialog(){
        CCSprite ccSprite=CCSprite.sprite("combat/combat.png");
        ccSprite.setAnchorPoint(0,0);
        addChild(ccSprite);

        CCSprite ccSprite_dialog=CCSprite.sprite("dialog/dialog.png");
        ccSprite_dialog.setPosition(650,400);
        ccSprite_dialog.setScale(2.0);
        addChild(ccSprite_dialog);

        CCLabel ccLabel=CCLabel.makeLabel("选项","",50);
        ccLabel.setColor(ccColor3B.ccWHITE);
        ccLabel.setPosition(640,520);
        addChild(ccLabel);

        CCLabel ccLabel_music=CCLabel.makeLabel("音乐:","",50);
        ccLabel_music.setColor(ccColor3B.ccGREEN);
        ccLabel_music.setPosition(540,400);
        addChild(ccLabel_music);


        CCMenu ccMenu = CCMenu.menu();
        CCSprite ccSprite_BackGame=CCSprite.sprite("dialog/back_game.png");
        CCSprite ccSprite_BackMenu=CCSprite.sprite("dialog/back_menu.png");

        CCMenuItemSprite ccMenuItemSprite = CCMenuItemSprite.item(ccSprite_BackGame,
                ccSprite_BackGame,this,"backGame");
        ccMenuItemSprite.setPosition(-250,-250);
        ccMenuItemSprite.setScale(2);
        ccMenu.addChild(ccMenuItemSprite);

        CCMenuItemSprite ccMenuItemSprite2 = CCMenuItemSprite.item(ccSprite_BackMenu,
                ccSprite_BackMenu,this,"backMenu");
        ccMenuItemSprite2.setPosition(250,-250);
        ccMenuItemSprite2.setScale(2);
        ccMenu.addChild(ccMenuItemSprite2);

        addChild(ccMenu);
    }

    public void backGame(Object item){
        CCDirector.sharedDirector().onResume();
        removeSelf();
    }

    public void backMenu(Object item){
        CCScene ccScene = CCScene.node();
        ccScene.addChild(new MenuLayer());
        CCFadeTransition ccFadeTransition = CCFadeTransition.transition(2,ccScene);
        CCDirector.sharedDirector().runWithScene(ccFadeTransition);
    }

}
