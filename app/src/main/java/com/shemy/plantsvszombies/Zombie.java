package com.shemy.plantsvszombies;

import org.cocos2d.actions.base.CCRepeatForever;
import org.cocos2d.actions.base.CCSpeed;
import org.cocos2d.actions.instant.CCCallFunc;
import org.cocos2d.actions.interval.CCAnimate;
import org.cocos2d.actions.interval.CCDelayTime;
import org.cocos2d.actions.interval.CCMoveTo;
import org.cocos2d.actions.interval.CCSequence;
import org.cocos2d.actions.interval.CCTintTo;
import org.cocos2d.nodes.CCAnimation;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.nodes.CCSpriteFrame;
import org.cocos2d.types.CGPoint;
import org.cocos2d.types.util.CGPointUtil;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Dzsom on 2018/11/20.
 */

public class Zombie extends CCSprite {

    private float speed=15;
    private  CGPoint end;
    private CombatLayer combatLayer;
    private int attack=10;
    private State state;
    private int HP=100;
    private boolean isSlow;

    public void dieBomb() {
        ArrayList<CCSpriteFrame> frames = new ArrayList<>();
        System.out.println("dieBomb");
        for (int i = 0; i <20 ; i++) {
            CCSpriteFrame ccSpriteFrame = CCSprite.sprite(String.format(Locale.CHINA,
                    "boomdie/Frame%02d.png",i)).displayedFrame();
            frames.add(ccSpriteFrame);
        }

        CCAnimation ccAnimation = CCAnimation.animationWithFrames(frames,0.15f);
        CCAnimate ccAnimate =CCAnimate.action(ccAnimation,true);
        CCCallFunc ccCallFunc=CCCallFunc.action(this,"remove");
        CCSequence ccSequence=CCSequence.actions(ccAnimate,ccCallFunc);
        runAction(ccSequence);
    }

    public void remove(){
        removeSelf();
    }


    public enum State{
        MOVE,ATTACK
    }


    public Zombie(CombatLayer combatLayer, CGPoint start,CGPoint end){

        super("zombies/zombies_1/walk/Frame00.png");
        setAnchorPoint(0.5f,0);
        setPosition(start);
        this.combatLayer=combatLayer;
        this.end=end;
        move();
    }

    public void move() {
        float t = CGPointUtil.distance(getPosition(),end)/speed;
        CCMoveTo ccMoveTo = CCMoveTo.action(t,end);
        CCCallFunc ccCallFunc = CCCallFunc.action(combatLayer,"end");
        CCSequence ccSequence = CCSequence.actions(ccMoveTo,ccCallFunc);
        if (isSlow){
            CCSpeed ccSpeed = CCSpeed.action(ccSequence,0.2f);
            runAction(ccSpeed);
        }else {
            runAction(ccSequence);
        }

        ArrayList<CCSpriteFrame> frames = new ArrayList<>();
        for (int i = 0; i <22 ; i++) {
            CCSpriteFrame ccSpriteFrame = CCSprite.sprite(String.format(Locale.CHINA,
                    "zombies/zombies_1/walk/Frame%02d.png",i)).displayedFrame();
            frames.add(ccSpriteFrame);
        }

        CCAnimation ccAnimation = CCAnimation.animationWithFrames(frames,0.1f);
        CCAnimate ccAnimate =CCAnimate.action(ccAnimation,true);
        CCRepeatForever ccRepeatForever = CCRepeatForever.action(ccAnimate);
        if (isSlow){
            CCSpeed ccSpeed = CCSpeed.action(ccRepeatForever,0.2f);
            runAction(ccSpeed);
        }else {
            runAction(ccRepeatForever);
        }
        setState(State.MOVE);
    }

    public void attaack(){
        ArrayList<CCSpriteFrame> frames = new ArrayList<>();
        for (int i = 0; i <21; i++) {
            CCSpriteFrame ccSpriteFrame = CCSprite.sprite(String.format(Locale.CHINA,
                    "zombies/zombies_1/attack/Frame%02d.png",i)).displayedFrame();
            frames.add(ccSpriteFrame);
        }

        CCAnimation ccAnimation = CCAnimation.animationWithFrames(frames,0.1f);
        CCAnimate ccAnimate = CCAnimate.action(ccAnimation,true);
        CCRepeatForever ccRepeatForever = CCRepeatForever.action(ccAnimate);
        if (isSlow){
            CCSpeed ccSpeed = CCSpeed.action(ccRepeatForever,0.2f);
            runAction(ccSpeed);
        }else {

            runAction(ccRepeatForever);

        }
        setState(State.ATTACK);
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public int getHP() {
        return HP;
    }

    public void setHP(int HP) {
        this.HP = HP;
    }

    public void hurrtCompute(int hurt){

        HP-=hurt;
        if (HP<0){
            HP=0;
        }
    }

    public void slow() {

        isSlow=true;
        stopAllActions();
        switch (getState()){

            case MOVE:
                move();
                break;

            case ATTACK:
                attaack();
                break;
        }

        CCTintTo ccTintTo1=CCTintTo.action(0.1f,ccc3(150,150,255));
        CCDelayTime ccDelayTime = CCDelayTime.action(3);
        CCTintTo ccTintTo2 = CCTintTo.action(0.1f,ccc3(255,255,255));
        CCCallFunc ccCallFunc = CCCallFunc.action(this,"normal");
        CCSequence ccSequence = CCSequence.actions(ccTintTo1,ccDelayTime,ccTintTo2,ccCallFunc);
        runAction(ccSequence);
    }

    public void normal(){
        isSlow=false;
        stopAllActions();
        switch (getState()){
            case MOVE:
                move();
                break;

            case ATTACK:
                attaack();
                break;
        }
    }

}
