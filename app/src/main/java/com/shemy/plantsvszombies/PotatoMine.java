package com.shemy.plantsvszombies;

import android.util.Log;

import org.cocos2d.actions.base.CCRepeatForever;
import org.cocos2d.actions.instant.CCCallFunc;
import org.cocos2d.actions.interval.CCAnimate;
import org.cocos2d.actions.interval.CCDelayTime;
import org.cocos2d.actions.interval.CCSequence;
import org.cocos2d.nodes.CCAnimation;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.nodes.CCSpriteFrame;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Dzsom on 2018/11/14.
 */

public class PotatoMine extends Plant {
    private CCSprite ccSprite_Bomb;
    private Boolean isUp;

    public PotatoMine() {
        super("plant/PotatoMine/PotatoMineNotReady.png",1);
        CCDelayTime ccDelayTime=CCDelayTime.action(10);
        CCCallFunc ccCallFunc=CCCallFunc.action(this,"potatoMineUp");
        CCSequence ccSequence=CCSequence.actions(ccDelayTime,ccCallFunc);
        runAction(ccSequence);
        setUp(false);
        setPrice(25);
        setHP(30);
    }

    public void potatoMineUp(){
        stopAllActions();
        setAnchorPoint(0.5f,0);
        ArrayList<CCSpriteFrame> frames = new ArrayList<>();
        for (int i = 0; i <8 ; i++) {
            CCSpriteFrame ccSpriteFrame = CCSprite.sprite(String.format(Locale.CHINA,"plant/PotatoMine/Frame%02d.png",i))
                    .displayedFrame();
            frames.add(ccSpriteFrame);
        }
        CCAnimation ccAnimation = CCAnimation.animationWithFrames(frames,0.2f);
        CCAnimate ccAnimate = CCAnimate.action(ccAnimation,true);
        CCRepeatForever ccRepeatForever = CCRepeatForever.action(ccAnimate);
        setUp(true);
        runAction(ccRepeatForever);
    }

    private int attack=200;

    public void boom(){
        ccSprite_Bomb=CCSprite.sprite("potatoMineBoom/boom.png");
        ccSprite_Bomb.setPosition(this.getPosition());
        getParent().addChild(ccSprite_Bomb,6);
        CCDelayTime ccDelayTime=CCDelayTime.action(1);
        setHP(0);
        CCCallFunc ccCallFunc=CCCallFunc.action(this,"remove");
        CCSequence ccSequence=CCSequence.actions(ccDelayTime,ccCallFunc);
        ccSprite_Bomb.runAction(ccSequence);
    }

    public int getAttack(){
        return attack;
    }

    public void remove(){
        Log.d("dd","remove");
        this.removeSelf();
        ccSprite_Bomb.removeSelf();
    }

    public Boolean getUp() {
        return isUp;
    }

    public void setUp(Boolean up) {
        isUp = up;
    }
}
