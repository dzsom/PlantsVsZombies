package com.shemy.plantsvszombies;

/**
 * Created by Dzsom on 2018/11/14.
 */

public class CherryBomb extends Plant {

    public CherryBomb() {
        super("plant/CherryBomb/Frame%02d.png",7);
        setPrice(150);
    }
}
