package com.shemy.plantsvszombies;

/**
 * Created by Dzsom on 2018/11/14.
 */

public class Peashooter extends ShootPlant {
    public Peashooter() {
        super("plant/Peashooter/Frame%02d.png",13);
        setPrice(100);
    }

    @Override
    public void createBullet(float t) {
        PeaBullet peaBullet = new PeaBullet(this);
    }
}
