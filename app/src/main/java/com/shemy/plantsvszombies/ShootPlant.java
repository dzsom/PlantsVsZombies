package com.shemy.plantsvszombies;

import org.cocos2d.actions.CCScheduler;

import java.util.ArrayList;

/**
 * Created by Dzsom on 2018/11/27.
 */

public abstract class ShootPlant extends Plant {

    private boolean isAttack;
    private ArrayList<Bullet> bullets;

    public ShootPlant(String format, int number) {
        super(format, number);
        bullets=new ArrayList<>();
    }

    public void attackZombie(){
        if (!isAttack){
            CCScheduler.sharedScheduler().schedule("createBullet",this,
                    5,false);
            isAttack=true;
        }
    }

    public void stopAttackZombie(){
        if (isAttack){
            CCScheduler.sharedScheduler().unschedule("createBullet",this);
            isAttack=false;
        }
    }

    public ArrayList<Bullet> getBullets() {
        return bullets;
    }

    public abstract void createBullet(float t);
}
